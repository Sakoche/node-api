const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const User = mongoose.model('User');

router.post('/signup', async (req, res) => {
  //const email  = req.get
  console.log(req.body)
  const {email, password} = req.body;
  try {
    const user = new User({email, password});
    await user.save(); //asyncronus
    const token = jwt.sign({ userId: user._id}, 'MY_SECRET_KEY')
    res.send({token: token})

  }catch(err){
    return res.status(422).send({err})
  }
 

});

module.exports = router;