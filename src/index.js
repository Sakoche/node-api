require('./models/User');
const express = require('express');
console.log(express)
const mongoose = require('mongoose');
const authRoute = require('./routes/authRoutes');
const app = express();
const requireAuth = require('./middlewares/requireAuth');

app.use(express.json());


app.use(authRoute);

const mongoUri = 'mongodb+srv://admin:adminadmin@tracker.qspux.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
mongoose.connect(mongoUri);
mongoose.connection.on('connected', () => {
    console.log('connected to mongo instancee')
})

mongoose.connection.on('error', (e) => {
    console.log('error while connected to mongo instancee'+ e)

});

app.get('/', requireAuth,  (req, res) => {
 res.send(`Your email : ${req.user.email}`);
});

app.listen(3005, () => {
    console.log('listening on poort 3005')
})